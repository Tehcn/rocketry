import numpy as np
import pygame as pg
from math import pi as PI
from util import R_deg


class Rocket:
    def __init__(self) -> None:
        self.thrust = 10
        self.mass = 3

        self.position = pg.Vector2()
        self.velocity = pg.Vector2()
        self.acceleration = pg.Vector2(0, -9.81)

        self.theta = 30

        self.width = 20
        self.height = 30

        self.scale = 1 / 100

    def update(self) -> None:
        F_D = 1

        F_t = self.thrust

        self.acceleration += np.sign(self.acceleration) * F_D
        self.acceleration += pg.Vector2(0, -9.81)
        self.acceleration += pg.Vector2(0, F_t)
        self.velocity += self.acceleration
        self.position += self.velocity

        print(self.position, self.velocity, self.acceleration)

        self.acceleration = pg.Vector2(0, 0)
        self.velocity = pg.Vector2(0, 0)

    def _draw_pos(self, screen: pg.Surface) -> pg.Vector2:
        x = (-self.position[0] * self.scale + screen.get_size()[0] / 2) - (
            self.width / 2
        )
        y = (-self.position[1] * self.scale + screen.get_size()[1] / 2) - (
            self.height / 2
        )
        p1 = R_deg(pg.Vector2(-1, 1), self.theta, pg.Vector2(x, y))
        p2 = R_deg(pg.Vector2(1, 1), self.theta, pg.Vector2(x, y))
        p3 = R_deg(pg.Vector2(1, -1), self.theta, pg.Vector2(x, y))
        p4 = R_deg(pg.Vector2(-1, -1), self.theta, pg.Vector2(x, y))
        return [p1, p2, p3, p4]

    def draw(self, screen: pg.Surface) -> None:
        pos = self._draw_pos(screen)
        pg.draw.polygon(screen, (255, 255, 255), pos, 1)
