import pygame as pg
import numpy as np

from rocket import Rocket


class Engine:
    def __init__(self) -> None:
        pg.init()
        self.clock = pg.time.Clock()
        pg.display.set_caption("rocketry")
        self.surface = pg.display.set_mode((1280, 720))
        self.loop = True
        self.rocket = Rocket()
        self.width = self.surface.get_size()[0]
        self.height = self.surface.get_size()[1]

    def run(self):
        while self.loop:
            for event in pg.event.get():
                if event.type == pg.QUIT:
                    self.loop = False
            self.update()
            self.draw()

    def update(self):
        self.rocket.update()

    def draw(self):
        self.surface.fill((0, 0, 0))
        self.axis()
        self.rocket.draw(self.surface)
        pg.display.update()

    def axis(self):
        center = (self.width / 2, self.height / 2)
        pg.draw.line(
            self.surface,
            (255 // 2, 255 // 2, 255 // 2),
            (0, center[1]),
            (self.width, center[1]),
        )
        pg.draw.line(
            self.surface,
            (255 // 2, 255 // 2, 255 // 2),
            (center[0], 0),
            (center[0], self.height),
        )


if __name__ == "__main__":
    engine = Engine()
    engine.run()
