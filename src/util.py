from math import sin, cos, pi as PI, radians as rad
from pygame import Vector2


def R_deg(p: Vector2, theta: float, c: Vector2) -> Vector2:
    """
    If theta is positive, the rotation will be clockwise (degrees)
    """
    return R(p, rad(theta), c)


def R(p: Vector2, theta: float, c: Vector2) -> Vector2:
    """
    If theta is positive, the rotation will be clockwise (radians)
    """
    return Vector2(
        (p.x - c.x) * cos(-theta) - (p.y - c.y) * sin(-theta) + c.x,
        (p.x - c.x) * sin(-theta) + (p.y - c.y) * cos(-theta) + c.y,
    )
